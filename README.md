# Koa Unless

This project provides a middleware to wrap another koa middleware, and if the path of incoming request is matched, then the wrapped middleware will be skiped.

## Enviroment Requirement

- Node.js: 16.14.2
- npm: 8.6.0

## Installation
Please use the following command to install it:

```bash=
npm install @hakalon/koa-unless --save
```

## Usage

### Set up rules

You need to create rules to tell unless which route should be bypassed, and it looks like this:
```javascript
// config.js

const config = [
    {
        methods: ['GET'],
        route: '^/dev/api/v1/users$'
    },
    {
        methods: ['POST', 'PUT'],
        route: '^/dev/api/v1/devices$'
    },
    {
        methods: ['GET'],
        route: '^/dev/api/v1/organizations/[\\d]{1,4}$'
    },
    {
        methods: ['GET'],
        route: new RegExp('^/dev/api/v1/notifications$')
    },
    {
        methods: ['*'],
        route: '^/dev/api/v1/sensors$'
    }
];

module.exports = config;
```
> Please be aware that `route` should be matched to `request.path`.  
> The detail of it please refer to [Koa official document](https://koajs.com/#request).

> Rule supports `*` symbol, and it equals to `['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS']`

### Using unless wrapper

```javascript
const unless = require('@knowledge-database/koa-unless');
const Koa = require('koa');
const app = new Koa();

const config = require('./config.js');

...

const someMiddleWare = (ctx, next) => {...};
const wrappedMiddleWare = unless(someMiddleWare, config);

// Now `someMiddleWare` will be bypassed if the route matches.
app.use(wrappedMiddleWare);

// You can set up the rules anomously too.
app.use(
    unless(someMiddleWare, [{ methods: ['*'], route: '^/dev/api/v1/users$' }])
);

```

## Contributing

Anyone contribution to this project is welcome.  
You can create a issue first, then send the merge request for it.