const Koa = require('koa');
const axios = require('axios');
const { expect, test, beforeEach, afterEach } = require('@jest/globals');
const unless = require('../src/unless');

let koaInstance;
let serverInstance;
const prefix = 'dev/api/v1';
const port = 3001;
const middleware = (ctx, next) => {
    ctx.status = 400;
    ctx.body = 'bad request';
    return next();
};

beforeEach(async () => {
    koaInstance = new Koa();
    koaInstance.use((ctx, next) => {
        ctx.status = 200;
        ctx.body = 'ok';
        return next();
    });
});

afterEach(() => {
    serverInstance?.close();
});

test('It should bypass the wrapped middleware if the route and method is matched', async () => {
    const config = [
        { methods: ['GET'], path: `^/${prefix}/users$` }
    ];

    koaInstance.use(unless(middleware, config));

    await new Promise((resolve) => {
        serverInstance = koaInstance.listen(port, () => {
            console.log(`Koa server start to listen at ${port}`);
            resolve();
        });
    });

    const { status: usrReqStatus, data: usrReqData } = await axios({
        url: `http://localhost:${port}/${prefix}/users`,
        method: 'GET'
    });

    expect(usrReqStatus).toBe(200);
    expect(usrReqData).toBe('ok');
});

test('It should bypass the wrapped middleware even if the config contains Regex', async () => {
    const config = [
        { methods: ['GET'], path: new RegExp(`^/${prefix}/roles$`) }
    ];

    koaInstance.use(unless(middleware, config));

    await new Promise((resolve) => {
        serverInstance = koaInstance.listen(port, () => {
            console.log(`Koa server start to listen at ${port}`);
            resolve();
        });
    });

    const { status: roleReqStatus, data: roleReqData } = await axios({
        url: `http://localhost:${port}/${prefix}/roles`,
        method: 'GET'
    });

    expect(roleReqStatus).toBe(200);
    expect(roleReqData).toBe('ok');
});

test('It should bypass the wrapped middleware even if the config contains * symbol', async () => {
    const config = [
        { methods: ['*'], path: `^/${prefix}/devices$` }
    ];

    koaInstance.use(unless(middleware, config));

    await new Promise((resolve) => {
        serverInstance = koaInstance.listen(port, () => {
            console.log(`Koa server start to listen at ${port}`);
            resolve();
        });
    });

    const { status: devGetReqStatus, data: devGetReqData } = await axios({
        url: `http://localhost:${port}/${prefix}/devices`,
        method: 'GET'
    });
    const { status: devPostReqStatus, data: devPostReqData } = await axios({
        url: `http://localhost:${port}/${prefix}/devices`,
        method: 'POST'
    });
    const { status: devPutReqStatus, data: devPutReqData } = await axios({
        url: `http://localhost:${port}/${prefix}/devices`,
        method: 'PUT'
    });
    const { status: devDelReqStatus, data: devDelReqData } = await axios({
        url: `http://localhost:${port}/${prefix}/devices`,
        method: 'DELETE'
    });
    const { status: devPatReqStatus, data: devPatReqData } = await axios({
        url: `http://localhost:${port}/${prefix}/devices`,
        method: 'PATCH'
    });
    const { status: devOptReqStatus, data: devOptReqData } = await axios({
        url: `http://localhost:${port}/${prefix}/devices`,
        method: 'OPTIONS'
    });

    expect(devGetReqStatus).toBe(200);
    expect(devGetReqData).toBe('ok');
    expect(devPostReqStatus).toBe(200);
    expect(devPostReqData).toBe('ok');
    expect(devPutReqStatus).toBe(200);
    expect(devPutReqData).toBe('ok');
    expect(devDelReqStatus).toBe(200);
    expect(devDelReqData).toBe('ok');
    expect(devPatReqStatus).toBe(200);
    expect(devPatReqData).toBe('ok');
    expect(devOptReqStatus).toBe(200);
    expect(devOptReqData).toBe('ok');
});

test('It should treate the incoming request path as lowercase if we set `ignoreCases` to true', async () => {
    const config = [
        { methods: ['GET'], path: `^/${prefix}/users$` },
        { methods: ['GET'], path: new RegExp(`^/${prefix}/roles$`) }
    ];

    koaInstance.use(unless(middleware, config, { ignoreCases: true }));

    await new Promise((resolve) => {
        serverInstance = koaInstance.listen(port, () => {
            console.log(`Koa server start to listen at ${port}`);
            resolve();
        });
    });

    const { status: usrReqStatus, data: usrReqData } = await axios({
        url: `http://localhost:${port}/${prefix}/USERS`,
        method: 'GET'
    });
    const { status: roleReqStatus, data: roleReqData } = await axios({
        url: `http://localhost:${port}/${prefix}/ROLES`,
        method: 'GET'
    });

    expect(usrReqStatus).toBe(200);
    expect(usrReqData).toBe('ok');
    expect(roleReqStatus).toBe(200);
    expect(roleReqData).toBe('ok');
});

test('It should not bypass the wrapped middleware if the route or method is not matched', async () => {
    const config = [
        { methods: ['GET'], path: `^/${prefix}/users$` },
        { methods: ['GET'], path: `^/${prefix}/roles$` }
    ];

    koaInstance.use(unless(middleware, config));

    await new Promise((resolve) => {
        serverInstance = koaInstance.listen(port, () => {
            console.log(`Koa server start to listen at ${port}`);
            resolve();
        });
    });

    let usrReqStatus = 'responseStatus';
    let usrReqData = 'responseData';

    try {
        await axios({
            url: `http://localhost:${port}/${prefix}/users`,
            method: 'POST'
        });
    } catch (error) {
        const { isAxiosError, response } = error;

        if (isAxiosError) {
            const { status, data } = response;
            usrReqStatus = status;
            usrReqData = data;
        }
    }


    let roleReqStatus = 'responseStatus';
    let roleReqData = 'responseData';

    try {
        await axios({
            url: `http://localhost:${port}/${prefix}/rolesNotMatched`,
            method: 'GET'
        });
    } catch (error) {
        const { isAxiosError, response } = error;

        if (isAxiosError) {
            const { status, data } = response;
            roleReqStatus = status;
            roleReqData = data;
        }
    }

    expect(usrReqStatus).toBe(400);
    expect(usrReqData).toBe('bad request');
    expect(roleReqStatus).toBe(400);
    expect(roleReqData).toBe('bad request');
});

test('It should throw an error if the config is invalid', async () => {
    const config = {
        invalidProp: 'invalid'
    };

    let errorObj;

    try {
        unless(middleware, config);
    } catch (error) {
        errorObj = error;
    }

    expect(errorObj).toHaveProperty('isUnlessError');
    expect(errorObj.isUnlessError).toBe(true);
    expect(errorObj).toHaveProperty('name');
    expect(errorObj.name).toBe('invalidRule');
    expect(errorObj).toHaveProperty('message');
    expect(errorObj.message).toBe('The parameter `rules` should be an array.');
});

test('It should throw an error if the property `methods` in a rule is not an array', async () => {
    const config = [
        { methods: 'GET', path: `^/${prefix}/users$` }
    ];

    let errorObj;

    try {
        unless(middleware, config);
    } catch (error) {
        errorObj = error;
    }

    expect(errorObj).toHaveProperty('isUnlessError');
    expect(errorObj.isUnlessError).toBe(true);
    expect(errorObj).toHaveProperty('name');
    expect(errorObj.name).toBe('invalidRule');
    expect(errorObj).toHaveProperty('message');
    expect(errorObj.message).toBe('There is an invalid rule of unless, the property `methods` must be an array.');
});

test('It should throw an error if the property `methods` contains any not acceptable method', async () => {
    const config = [
        { methods: ['NOTACCEPT'], path: `^/${prefix}/users$` }
    ];

    let errorObj;

    try {
        unless(middleware, config);
    } catch (error) {
        errorObj = error;
    }

    expect(errorObj).toHaveProperty('isUnlessError');
    expect(errorObj.isUnlessError).toBe(true);
    expect(errorObj).toHaveProperty('name');
    expect(errorObj.name).toBe('notAcceptableMethod');
    expect(errorObj).toHaveProperty('message');
    expect(errorObj.message).toBe('There is an not acceptable method `NOTACCEPT` in rules.');
});