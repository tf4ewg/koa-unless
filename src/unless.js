const METHODS = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'];

/**
 * Determine whether or not the request path is matched to the given path
 *
 * @param {string} requestPath
 * @param {RegExp|string} path
 * @returns {boolean}
 */
const matchPath = (requestPath, path) => {
    if (typeof path === 'string') {
        return requestPath.match(path) !== null;
    } else if (path instanceof RegExp) {
        return path.test(requestPath);
    } else {
        return false;
    }
};

/**
 * Return a middleware which will bypass the passed middleware if the any one of rules is matched
 *
 * @param {function}    middleware          Koa middleware
 * @param {object[]}    rules               unless rules
 * @param {string}      rules[].path        target request url
 * @param {string[]}    rules[].methods     target request methods, it support `*` symbol to include all methods that are defined in `METHODS`
 * @param {object}      options
 * @param {boolean}     options.ignoreCases whether or not to treat the incoming request path as lowercase
 *
 * @returns {function} Koa middleware
 */
const unless = (middleware, rules, options = {}) => {
    const { ignoreCases = false } = options;
    const unlessMap = new Map(METHODS.map((method) => [method.toUpperCase(), new Set()]));

    if (!Array.isArray(rules)) {
        const error = new Error('The parameter `rules` should be an array.');
        error.isUnlessError = true;
        error.name = 'invalidRule';
        throw error;
    }

    for (const { path, methods } of rules) {
        if (!Array.isArray(methods)) {
            const error = new Error('There is an invalid rule of unless, the property `methods` must be an array.');
            error.isUnlessError = true;
            error.name = 'invalidRule';
            throw error;
        }

        for (const method of methods) {
            const upperMethod = method?.toUpperCase();
            if (upperMethod === '*') {
                unlessMap.forEach((value) => value.add(path));
                break;
            } else if (METHODS.includes(upperMethod)) {
                unlessMap.get(upperMethod).add(path);
            } else {
                const error = new Error(`There is an not acceptable method \`${method}\` in rules.`);
                error.isUnlessError = true;
                error.name = 'notAcceptableMethod';
                throw error;
            }
        }
    }

    unlessMap.forEach((set, key) => unlessMap.set(key, [...set.values()]));

    return async (ctx, next) => {
        const bypassPaths = unlessMap.get(ctx.method.toUpperCase());
        const isMatched = bypassPaths.some((path) => matchPath(ignoreCases ? ctx.path.toLowerCase() : ctx.path, path));

        if (isMatched) {
            return next();
        } else {
            return middleware(ctx, next);
        }
    };
};

module.exports = unless;