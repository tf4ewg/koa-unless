module.exports = {
    env: {
        es2021: true,
        node: true
    },
    extends: 'eslint:recommended',
    parserOptions: {
        'ecmaVersion': 'latest',
        'sourceType': 'module'
    },
    rules: {
        'indent': ['error', 4, { SwitchCase: 1 }],
        'quotes': ['error', 'single'],
        'semi': ['error', 'always'],
        'semi-spacing': ['error'],
        'comma-dangle': ['error', 'never'],
        'comma-spacing': 'error',
        'key-spacing': 'error',
        'arrow-spacing': 'error',
        'space-infix-ops': 'error',
        'brace-style': ['error', '1tbs', { 'allowSingleLine': true }],
        'camelcase': 'error',
        'new-cap': 'error',
        'space-before-blocks': 'error',
        'no-var': 'error',
        'no-unused-vars': 'error',
        'no-unreachable': 'error',
        'computed-property-spacing': ['error', 'never'],
        'curly': ['error', 'all'],
        'no-unneeded-ternary': 'error',
        'no-trailing-spaces': 'error',
        'no-empty': 'error',
        'no-useless-catch': 'off'
    }
};